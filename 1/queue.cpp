#include "stdafx.h" 
#include "queue.h"

void initQueue(queue* q, unsigned int size)
{
	q->intQueue = new unsigned int [size];
	q->queueSize = size;
	q->count = 0;
}

void cleanQueue(queue* q)
{
	delete[] q->intQueue;
}

void enqueue(queue* q, unsigned int newValue)
{
	if (q->count + 1 <= q->queueSize)
	{
		if (q->count != 0)
		{
			for (unsigned int i = q->count; i > 0; i--)
			{
				q->intQueue[i] = q->intQueue[i - 1];
			}
		}
		q->intQueue[0] = newValue;
		q->count++;
	}
}

int dequeue(queue* q)
{
	if (q->count != 0)
	{
		q->count--;
		return q->intQueue[q->count];
	}
	else
	{
		return -1;
	}
}