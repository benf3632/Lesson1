#ifndef QUEUE_H
#define QUEUE_H

/* a queue contains positive integer values. */
typedef struct queue
{
	unsigned int* intQueue; //the queue
	unsigned int queueSize; //the size of the queue
	unsigned int count; //the position in the queue
} queue; 

void initQueue(queue* q, unsigned int size); //initiates a queue to max size of queue
void cleanQueue(queue* q); //cleans a queue

void enqueue(queue* q, unsigned int newValue); //inserts a value to the end of the queue
int dequeue(queue* q); // return element in top of queue, or -1 if empty

#endif /* QUEUE_H */