#pragma once

typedef struct list
{
	unsigned int value;
	list* next;
}list;

void initList(list** linkedList); //init the linked list

void add(list** linkedList, unsigned int newValue); //adds to the list

void remove(list** linkedList); //removes from the list

void cleanList(list** linkedList); //cleans the list