#include "stdafx.h"
#include "utils.h"

void reverse(int* nums, unsigned int size)
{
	stack s;
	initStack(&s);
	for (unsigned int i = 0; i < size; i++)
	{
		push(&s, nums[i]);
	}

	for (unsigned int i = 0; i < size; i++)
	{
		nums[i] = s.linkedList->value;
		pop(&s);
	}
	cleanStack(&s);
}

int* reverse10()
{
	int* nums = new int[10];
	for (int i = 0; i < 10; i++)
	{
		std::cin >> nums[i];
		getchar();
	}
	reverse(nums, 10);
	return nums;
}
