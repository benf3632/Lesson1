#include "stdafx.h"
#include "stack.h"

void initStack(stack* s)
{
	s->linkedList = 0;
}

void push(stack* s, unsigned int element)
{
	add(&s->linkedList, element);
}

int pop(stack* s)
{
	int retElement;
	if (s->linkedList)
	{
		retElement = s->linkedList->value;
		remove(&s->linkedList);
		return retElement;
	}
	else
	{
		return -1;
	}
}

void cleanStack(stack* s)
{
	cleanList(&s->linkedList);
}

	