#include "stdafx.h"
#include "linkedList.h"

void initList(list** linkedList)
{
	*linkedList = new list;
	(*linkedList)->next = nullptr;
}

void add(list** linkedList, unsigned int newValue)
{
	list* temp = 0;
	if (*linkedList)
	{
		temp = new list;
		temp->value = newValue;
		temp->next = *linkedList;
		*linkedList = temp;
	}
	else
	{
		initList(linkedList);
		(*linkedList)->value = newValue;
	}
}

void remove(list** linkedList)
{
	list* temp = 0;
	if (*linkedList)
	{
		temp = (*linkedList)->next;
		delete *linkedList;
		*linkedList = temp;
	}
}

void cleanList(list** linkedList)
{
	list* curr = 0;
	list* temp = 0;
	
	if (*linkedList)
	{
		cleanList(&((*linkedList)->next));
		remove(linkedList);
	}
}